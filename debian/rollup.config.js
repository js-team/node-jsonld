const resolve = require('@rollup/plugin-node-resolve').nodeResolve;
const commonjs = require('@rollup/plugin-commonjs');
//import {terser} from "rollup-plugin-terser";

const ROLLUP_PRODUCTION = (process.env.ROLLUP_PRODUCTION === 'true');

module.exports = {
	plugins: [
		resolve({
			modulePaths: ['/usr/share/nodejs','/usr/lib/nodejs']
		}),
		commonjs(),
//		ROLLUP_PRODUCTION ? terser() : false,
	],
};
