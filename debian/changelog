node-jsonld (8.3.2-1) experimental; urgency=medium

  * declare compliance with Debian Policy 4.6.2
  * update copyright info: update coverage
  * drop all patches, obsoleted by upstream changes
  * update build rules;
    build-depend on node-rollup-plugin-commonjs
    node-rollup-plugin-json node-rollup-plugin-node-resolve rollup
    (not node-babel7 node-babel-loader node-webpack-merge webpack);
    closes: bug#958682, #1003502, thanks to yadd and Caleb Adepitan

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 25 Dec 2023 19:19:51 +0100

node-jsonld (4.0.1-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Yadd ]
  * declare compliance with Debian Policy 4.6.0
  * use debhelper compatibility level 13 (not 12)
  * stop depend on nodejs

  [ Nilesh ]
  * use NodeJS module core-js v3;
    tighten build-dependency on node-core-js

  [ Jonas Smedegaard ]
  * stop depend on node-xmldom, no longer used upstream
  * drop patch cherry-picked upstream now applied
  * update and unfuzz patches
  * tighten (build-)dependency on node-rdf-canonize
  * stop build-depend on nodejs, not explicitly used during build;
    have autopkgtest depend on nodejs
  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage
  * simplify source helper script copyright-check

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 08 Nov 2021 22:35:51 +0100

node-jsonld (3.2.0-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * modernize source script copyright-check
  * copyright: update coverage
  * declare compliance with Debian Policy 4.5.1
  * replace patch 2005 with upstream merge-request 361
  * add patch 1002 to drop obsolete object.fromentries shim
  * unfuzz patches
  * (build-)depend on node-canonixalize node-lru-cache

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 21 Nov 2020 20:29:37 +0100

node-jsonld (1.6.2-6) unstable; urgency=medium

  * re-release targeted unstable

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 May 2020 00:09:54 +0200

node-jsonld (1.6.2-5) experimental; urgency=medium

  * stop build-depend on node-babel-plugin-transform-object-rest-spread
    node-babel-plugin-transform-runtime;
    really really closes: bug#960101, thanks (again) to Pirate Praveen
  * revive patch 2005
  * update patch 2003 to favor /usr/share paths,
    and drop deep path into core-js symlinks from babel-runtime

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 09 May 2020 17:09:16 +0200

node-jsonld (1.6.2-4) experimental; urgency=medium

  * use babel7 presets;
    drop patches 2001 2005;
    unfuzz patch 2003;
    build-depend on node-babel7 node-babel-plugin-transform-runtime
    node-babel-plugin-transform-object-rest-spread
    (not node-babel-cli node-babel-preset-env
    node-babel-plugin-transform-object-rest-spread
    node-babel-plugin-transform-runtime);
    tighten build-dependency on node-babel-loader
    closes: bug#960101, thanks to Pirate Praveen

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 09 May 2020 15:44:33 +0200

node-jsonld (1.6.2-3) unstable; urgency=medium

  [ Nilesh ]
  * fix core-js paths
  * remove duplicate entries
  * set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse
  * use debhelper compatibility level 12 (not 9);
    build-depend on debhelper-compat (not debhelper)

  [ Jonas Smedegaard ]
  * fix: tighten build-dependency on node-core-js
  * extend module paths to include /usr/share/nodejs
    and suspicious node-babel-runtime subpath;
    closes: bug#952375
  * add DEP-5 patch header for patch 2004
  * use execute_before_dh_auto_clean (not override_dh_auto_clean)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 17 Mar 2020 19:09:21 +0100

node-jsonld (1.6.2-2) unstable; urgency=medium

  * Drop patch 2002, and tighten to depend on recent webpack.
    Closes: Bug#933592. Thanks to Pirate Praveen.
  * Unfuzz patch 2003.
  * Mark autopkgtest as superficial.
  * Install nodejs code under /usr/share (not /usr/lib).

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 28 Aug 2019 20:56:43 +0200

node-jsonld (1.6.2-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 4.4.0.
  * Unfuzz patches.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 15 Jul 2019 00:52:05 -0300

node-jsonld (1.5.3-1) unstable; urgency=medium

  [ upstream ]
  * New release(s).

  [ Jonas Smedegaard ]
  * Update watch file:
    + Simplify regular expressions.
    + Rewrite usage comment.
  * Update patch 2002 to revive loading webpack itself.
  * Unfuzz patches.
  * Tighten to (build-)depend versioned on node-rdf-canonize.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 25 Feb 2019 20:38:17 +0100

node-jsonld (1.4.0-4) unstable; urgency=medium

  * Update copyright info: Fix source URLs.
  * Fix depend (not only build-depend) on node-semver.
  * Mark libjs-jsonld as Multi-Arch: foreign.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 19 Jan 2019 12:24:56 +0100

node-jsonld (1.4.0-3) unstable; urgency=medium

  * Fix build-depend on node-rdf-canonize.
    Closes: Bug#919237. Thanks to Santiago Vila.
  * Unfuzz patch 2002.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Jan 2019 03:02:14 +0100

node-jsonld (1.4.0-2) unstable; urgency=medium

  * Add patch 1001 to fix odd whitespace breaking Webpack 3.
  * Fix and limit patch 2001 to downgrade to use Babel 6
    (not avoid Babel altogether).
  * Transpile with Babel to support Nodejs 6.
    Build-depend on node-babel-cli node-babel-preset-env node-semver.
    (Build-)depend on (not very) recent nodejs.
  * Add patch 2002 to downgrade to use Webpack 3.
  * Add patch 2003 to have webpack use system shared modules.
  * Provide binary package libjs-jsonld.
    Build-depend on node-babel-loader
    node-babel-plugin-transform-object-rest-spread
    node-babel-plugin-transform-runtime node-core-js
    node-webpack-merge webpack.
  * Provide zlib and brotli pre-compressed files.
    Build-depend on brotli pigz.
  * Update TODOs.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 13 Jan 2019 14:16:43 +0100

node-jsonld (1.4.0-1) unstable; urgency=low

  * Initial release.
    Closes: Bug#919024.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Jan 2019 02:11:23 +0100
